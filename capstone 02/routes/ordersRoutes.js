const express = require('express');
const usersControllers = require('../controllers/usersControllers.js')
const productsControllers = require('../controllers/productsControllers.js')
const ordersControllers = require('../controllers/ordersControllers.js')
const router = express.Router();
const auth = require("../auth.js")

// CREATE ORDER (ADMIN ONLY)
router.post("/addtocart", auth.verify, ordersControllers.addToCart)

// RETRIEVE ALL ORDERS (ADMIN ONLY)
router.get("/allOrders", auth.verify, ordersControllers.getAllOrders)

// RETRIEVE USER ORDER
router.get("/viewCart", auth.verify, ordersControllers.viewCart)

// UPDATE PRODUCT QUANTITY
router.patch("/updateProductQuantity", auth.verify, ordersControllers.updateProductQuantity)
module.exports = router;
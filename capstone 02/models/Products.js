const mongoose = require("mongoose");

// PRODUCT MODEL
const productSchema = new mongoose.Schema({

	productName: {
		type: String,
		required: [true, "Product name is required!"]
	},

	description: {
		type: String,
		required: [true, "Description is required!"]
	},

	price: {
		type: Number,
		required: [true, "Price is required!"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: String,
		default: new Date()
	},

	userOrders: [{
		userId: {
			type: String,
			required: [true, "User identification is required!"]
		},

		orderId: {
			type: String,
			required: [true, "Oder identification is required!"]
		}
	}]

}) //end PRODUCT MODEL

const Products = mongoose.model("Product", productSchema);
module.exports = Products;
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 4001;
const usersRoutes = require('./routes/usersRoutes.js');
const productsRoutes = require('./routes/productsRoutes.js');
const ordersRoutes = require ('./routes/ordersRoutes.js');
const cors = require('cors');

// mongoDb connection
mongoose.connect("mongodb+srv://admin:admin@batch288villacacan.yzbkoln.mongodb.net/ecommerce?retryWrites=true&w=majority",
 {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

    db.on("error", console.error.bind(console, "Error, can't connect to the database!"));

    db.once("open", () => console.log("We are now connected to the database!"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// reminder that we are going to use this for the sake of the bootcamp
app.use(cors());

// add the routing of the routes from the usersRoutes
app.use('/users', usersRoutes)
app.use('/products', productsRoutes)
app.use('/orders', ordersRoutes)
app.listen(port, () =>{
    console.log(`The server is running at port ${port}`)
});



import  {useState, useEffect} from 'react';
import  {useParams, useNavigate} from 'react-router-dom';
import  Swal2 from 'sweetalert2';
import  {Container, Row, Col, Button, Card, FloatingLabel, Form} from 'react-bootstrap';

export default function ProductView(){
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
    const [qty, setQuantity] = useState('');
	const navigate = useNavigate();
    const totalAmount = qty*price
	const { productId } = useParams();

	useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [])


	const addtocart = (productId) => {
        console.log(productId);
        console.log(qty);
        console.log(totalAmount);
		fetch(`${process.env.REACT_APP_API_URL}/orders/addtocart`, {
			method : 'POST',
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`,
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				productId : productId,
                quantity: qty,
                amount: totalAmount
			})
		})
		.then(response => response.json())
		.then(data => {
            console.log(data)
			if(data){
				Swal2.fire({
					title: 'Successfully added to cart',
					icon: 'success'
				})
				navigate('/allProducts');
			}else{
				Swal2.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}


return(
	<Container>
		<Row>
			<Col>
				<Card> 
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>

				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>P {price}</Card.Text>

                        <FloatingLabel controlId="floatingInput" label="Quantity" className="mb-3" >
                            <Form.Control type="number"
                             placeholder="Quantity" value={qty}
                             onChange = {event => {
                                setQuantity(parseFloat(event.target.value))
                            }}
                            />
                        </FloatingLabel>

				        <Button variant="primary" onClick = {() => addtocart(productId)}>Checkout</Button>
				      </Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>
	)
}
import {Container, Row, Col, Button, Form} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import { Navigate, Link, useNavigate } from 'react-router-dom';
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';


export default function Login(){
    
    const [isDisabled, setIsDisabled] = useState(true);
    const [email, typeEmail] = useState('');
    const [password, typePassword] = useState('');

    const {user,setUser} = useContext(UserContext);
    const navigate = useNavigate();
    
    useEffect(() => {
        if(email && password){   
            setIsDisabled(false) 
        } else {
            setIsDisabled(true)
        }
    }
    ,[email, password])

    const login = (e) => {
        e.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json',
                'Accept': 'application/json'
            },
            body : JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(result => result.json())
        .then(data => {
            if(data === false){
                Swal2.fire({
                    title: 'Authentication failed!',
                    icon: 'error',
                    text: 'Check your login details and try again!'
                })
            }else{
                
                localStorage.setItem('token', data.auth);

                retrieveUserDetails(data.auth);
                console.log(data.auth)
                Swal2.fire({
                    title : 'Login Successful',
                    icon : 'success',
                    text: 'Welcome to CoBo!'
                })

                navigate('/')
            }
        })
    }


    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(data => {
            console.log(data);
            setUser({
                id : data._id,
                isAdmin: data.isAdmin
            });

        })
    }
    
    return (
        
        <Container className='mt-5'>
            <Row>
                <Col className='col-6 mx-auto'>
                    <h1 className='text-center'>Login</h1>
                    <Form onSubmit = {event => login(event)}>
                        <Form.Group className='mb-3' controlId='loginEmail>'>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" value ={email} onChange = {event => {typeEmail(event.target.value); }} placeholder = "Enter email"/>
                        </Form.Group>

                        <Form.Group className='mb-3' controlId='password>'>
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value = {password} onChange = {event => {typePassword(event.target.value)}} placeholder="Password"/>
                        </Form.Group>

                        <p>No account yet? <Link to ='/register'>Sign up here.</Link></p>

                        <Button variant = "success" disabled = {isDisabled} type = "submit">Login</Button>
                    </Form>
                </Col>
            </Row>
            </Container>
         
              
    )
}
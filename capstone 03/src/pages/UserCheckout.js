import {Container, Row, Col, Card, Button} from 'react-bootstrap'


export default function UserCheckout(){
   
    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const {productId} = useParams();
    const {user, setUser} = useContext(UserContext);

    const navigate = useNavigate();
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(()=>{
		if (productName !== '' || description !== '' || price !== ''){

			setIsDisabled(false);

		}else{
			setIsDisabled(true);
		}
    }, [productName, description, price]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(result => result.json())
            .then(data => {
                console.log(data);
                setProductName(data.productName);
                setDescription(data.description);
                setPrice(data.price)
            })
    }, [productId])

    function updateToProduct(event){
        event.preventDefault()

        if (user.isAdmin === true){

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'PATCH',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productName : productName,
                description : description,
                price : price
            })
        })
        .then (result => result.json())
        .then (data => {
            if (data === false){
                Swal2.fire({

                    title: 'Failed to update product',
                    icon: 'error',
                    text: 'Try again!'

                })
            } else {
                Swal2.fire({
                    title: 'Product updated successfully!',
                    icon: 'success',
                })

                navigate(`/productDetails/${productId}`)
            }
        })

    } else {
        navigate('/PageNotFound')
    }



}
}
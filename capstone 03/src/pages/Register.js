import {Container, Row, Col, Button, Form} from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import {Link, useNavigate, Navigate} from 'react-router-dom';

import Swal2 from 'sweetalert2';

export default function Register(){
	// const [fn, setFn] = useState('');
	// const [ln, setLn] = useState('');
	// const [mn, setMn] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	//we consume the setUser function from the UserContext
	const {user, setUser} = useContext(UserContext);


	//we containe the useNavigate to navigate variable;
	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	//we have to use the useEffect in enabling the submit button
	useEffect(()=>{
		if (email !== '' && password1 !=='' && password2 !== '' && password1 === password2 && password1.length > 0){

			setIsDisabled(false);

		}else{
			setIsDisabled(true);
		}


	}, [email, password1, password2]);

	//function that will be triggered once we submit the form
	function register(event){
		//it prevents our pages to reload when submitting the forms
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
                password: password1
                
			})
		})
		.then(result => result.json())
        .then(data => {
            console.log(data);
            if (data === false){
                Swal2.fire({
                    title : 'Registration failed',
                    icon : 'error',
                    text : 'Check your details and try again.'
                })
            } else {
           
                localStorage.setItem('token', data.auth)
                registerUser(data.auth)
                
                Swal2.fire({
                    title : 'Registration Successful',
                    icon : 'success',
                    text : 'Welcome to CoBo!'
                })

                navigate('/')
            }
        })


		navigate('/')
	}

	const registerUser = (token) => {
		fetch (`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				
                email: email,
                password: password1,
                
			}) 
		})

		.then(result => result.json())
		.then(data => {
			setUser({
				id : data._id,
				isAdmin: data.isAdmin
			});
		});

	
	}

return(
	
	<Container className = 'mt-5'>
		<Row>
			<Col className = 'col-6 mx-auto'>
				<h1 className = 'text-center'>Sign Up</h1>
				<Form onSubmit = {event => register(event)}>
				      
					  <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	value = {email}
				        	onChange = {event => {
				        		console.log(event)
				        		setEmail(event.target.value)
				        	}}
				        	placeholder="Enter email" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password1}
				        	onChange = {event => setPassword1(event.target.value)}
				        	placeholder="Password" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				        	placeholder="Retype your nominated password" />
				      </Form.Group>

				      <p>Have an account already? <Link to = '/login'>Log in here.</Link></p>


				      <Button variant="dark" type="submit"
				      		disabled = {isDisabled}>
				        Sign Up
				      </Button>
				    </Form>

			</Col>
		</Row>
	</Container>
	
	)
}
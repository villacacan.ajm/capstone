import {Link} from 'react-router-dom'
import {Container, Row, Col, Image} from 'react-bootstrap'
import "bootstrap/dist/css/bootstrap.css"

export default function PageNotFound(){
    return (
        
        <Container className='mt-5 text-primary'>
            <Row>
                <Col className='text-center'>
                    <h1 className='my-3'>Whoops!</h1>
                    <h2 className='my-3'>404 Page Not Found</h2>
                    <h2 className='my-3'>Looks like this page went on vacation.</h2>
                    <div className='text-dark'>
                    <Image rounded fluid src='https://daysinnboardwalk.com/wp-content/uploads/2018/11/cool-dog-in-sunglasses-drinking-frozen-drink_258482924-768x635.jpg' />
                    <h3 className='my-3'> Try our <Link className="text-decoration-none" to = '/'>homepage</Link> or <Link className="text-decoration-none" to = '/allProducts'>products</Link> instead</h3>
                    </div>
                 </Col>
            </Row>
        </Container>
        
        
    )
}
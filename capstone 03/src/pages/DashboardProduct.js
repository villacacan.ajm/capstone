import { Button } from 'react-bootstrap';
import ProductCard from '../components/ProductCard'
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import DashboardProductCard from '../components/DashboardProductCard';

export default function Products(){

	const [products, setProducts] = useState([]);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(result => result.json())
		.then(data => {
			// console.log(data)
			setProducts(data.map(product => {
				return(
					<DashboardProductCard key = {product._id} productProp = {product}/>
				)
			}))
		})
	}, [])

	return(
		<>
		
		<h1 className='mt-3 text-center'>Products</h1>
		<div className="text-center">
		<Button className='text-center m-3' as = {Link} to = {'/activeProducts'}>Active Products</Button>
		</div>
		{products}
		
		</>
		
	)
	
}
import {Container, Row, Col, Card, Button} from 'react-bootstrap'

export default function Highlights(){
    return(
        <Container className = 'mt-5'>
            <Row className = 'm-auto'>
            <Col className = 'col-12 col-md-4 mt-3 p-0'>
                <Card className = 'cardHighlight'>
                    <Card.Body>
                        <Card.Title>Learn from home</Card.Title>
                        <Card.Text>
                                Some quick example text to build on the card title and make up the
                                bulk of the card's content.
                        </Card.Text>
                        
                    </Card.Body>
                </Card>
            </Col>
            <Col className = 'col-12 col-md-4 mt-3 px-1 px-md-1 px-sm-0'>
                <Card className = 'cardHighlight'>
                    <Card.Body>
                        <Card.Title>Study Now, Pay Later</Card.Title>
                        <Card.Text>
                                Some quick example text to build on the card title and make up the
                                bulk of the card's content.
                        </Card.Text>
                        
                    </Card.Body>
                </Card>
            </Col>
            <Col className = 'col-12 col-md-4 mt-3 p-0'>
                <Card className = 'cardHighlight'>
                    <Card.Body>
                        <Card.Title>Be part of our community</Card.Title>
                        <Card.Text>
                                Some quick example text to build on the card title and make up the
                                bulk of the card's content.
                        </Card.Text>
                        
                    </Card.Body>
                </Card>
            </Col>
            </Row>
        </Container>
        
        
    )
}


import {useContext} from 'react';
import UserContext from '../UserContext.js';
import {Container, Row, Col} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
export default function AppNavBar(){

	const {user} = useContext(UserContext);


	return(
	<>
       <div border="primary">
       <Navbar fixed='bottom' bg="white" className='text-color-dark h5 p-3 m-0'>
	        <Container>
	          <Navbar.Brand id = "footer">&copy; CoBo - Elevate Your Shopping Experience, Beyond Boundaries</Navbar.Brand>
	        </Container>
	    </Navbar>
        </div>
                
	      
	</>


		)

}
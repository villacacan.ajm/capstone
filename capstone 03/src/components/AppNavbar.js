import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext.js';

export default function AppNavBar(){

	const {user} = useContext(UserContext);


	return(
	<>
	      <Navbar sticky='top' bg="white" className='text-color-dark h5'>
	        <Container>
	          <Navbar.Brand id = "accentColor" as = {Link} to = '/' ><img
			  src='https://i.ibb.co/5F79xd8/cobo-logo.png'
			  width="30"
			  height="30"
			  className='d-inline-blobk align-center' 
			  />CoBo</Navbar.Brand>
	          <Nav className="ms-auto">
	            <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
	            <Nav.Link as = {NavLink} to = '/allProducts'>Products</Nav.Link>
	            {
	            	user.isAdmin === true

	            	?

					<>
					<Nav.Link as = {NavLink} to = '/dashboard'>Dashboard</Nav.Link>
	            	<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
					</>
            	           	
	            	:

					localStorage.getItem('token') === null

					?

					<>
	            		<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
						<Nav.Link as = {NavLink} to = '/register'>Sign Up</Nav.Link>
						
	            	</> 
					:
					<>
						<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
					</>
					
				}


	          </Nav>
	        </Container>
	      </Navbar>
	      
	</>


		)

}
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Swal2 from 'sweetalert2';
import Table from 'react-bootstrap/Table';

export default function DashboardProductCard(props){
   
    const {productId} = useParams();
    const {user} = useContext(UserContext);
    const navigate = useNavigate()

    const [name, setProductName] = useState('');
    const [description, setDescription] = useState('');
    
    const {_id, productName} = props.productProp;
    
    const [price, setPrice] = useState('')
    
    const [active, setIsActive] = useState('');

    useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'GET',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
		.then(result => result.json())
		.then(data => {
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
            setIsActive(data.isActive);
            console.log(data);
		})

	}, [])

    useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
            setIsActive(data.isActive);
		})

	}, [])

    const changeStatus = (e) => {
        e.preventDefault()
   
        if (user.isAdmin === true){

            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/productStatus`, {
                method: 'PATCH',
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    isActive : active
                })
            })
            .then (result => result.json())
            .then (data => {
                if (data === false){
                    Swal2.fire({
                        title: 'Product is now back on shelf!',
                        icon: 'success',
                        text: 'product activated!'
                    })
                } else {
                    Swal2.fire({

                        title: 'Good riddance! Product is now archived!',
                        icon: 'info',
                        text: 'product archived!'

                    })
                
                    navigate(`/productDetails/${productId}`)
                }
            })

        } else {
            navigate('/PageNotFound')
        }

    }

    return (

        <Container className='mx-auto my-3 text-center d-block' >
            <Row className='d-block'>
                <Col className='d-block' aling='center'>
                <Card className='d-block' border="primary">
                    <Card.Body>
                        <Card.Title>{productName}</Card.Title>
                        {/* <Card.Subtitle className='mt-3'>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                           
                        <Card.Title>Price</Card.Title>
                        <Card.Subtitle>Php {price}</Card.Subtitle> */}
                        
                        <Card.Header>
                        <Button className='m-1' as = {Link} to = {`/productDetails/${_id}`}>Details</Button>
                        </Card.Header>
                    </Card.Body>
                </Card>
                </Col>

                <Col>
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                      
                        <td>{productName}</td>
                        <td>{description}</td>
                        <td>

                        <Button className="m-1" variant="success" as = {Link} to = {`/updateProduct/${_id}`}>Update</Button>

                        {
                            active && user.isAdmin === true
                           ?
                           <Button className="m-1" variant="primary" onClick={event => changeStatus(event)}>Archive</Button>
                           :
                           <></>
                           
                       }

                        
                        {
                            !active && user.isAdmin === true
                           ?
                            <Button className="m-1" variant="primary" onClick={event => changeStatus(event)}>Activate</Button>
                            :
                            <></>
                            
                       }
                        
                        </td>
                       
                       
                        </tr>
                    </tbody>
                </Table>   
                </Col>
            </Row>
        </Container>
    )
}